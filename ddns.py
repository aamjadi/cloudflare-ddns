#!/usr/bin/python3
import json
import os
import time
import sys

import requests

ddns = ""  # The dns record we want to update on Cloudflare.

zone_identifier = ""  # your Zone ID

# zone id for the dns record we want to update.
# (find your dns id on dnslist => https://api.cloudflare.com/#dns-records-for-a-zone-list-dns-records)
dns_id = ""

email = "" # your registered email address on cloudflare
authkey = ""  # your X-Auth-Key

# Location of this script.
dir_path = os.getcwd()



id_url = 'https://api.cloudflare.com/client/v4/zones/{}/dns_records?type=A&page=1&per_page=20&order=type&direction=desc&match=all'

def log(text, error=False):
    file = open(dir_path + "/ddns.log", 'a')
    ltype = "ERROR: " if error else ""
    file.write("{}{}\n\n".format(ltype, text))
    file.close()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == 'id':
            headers = {"X-Auth-Email": email, "X-Auth-Key": authkey, 
                        "Content-Type": "application/json"}
            req_ids = requests.get(id_url.format(zone_identifier), headers=headers)
            req = req_ids.json()
            if req['success']:
                for i in req['result']:
                    print(i['name'], i['id'])
            exit()
    while True:
        try:
            # Discover your DNS IP address.
            req_ip = requests.get(
                "https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}".format(zone_identifier, dns_id),
                headers={"X-Auth-Email": email, "X-Auth-Key": authkey, "Content-Type": "application/json"})
            ip = req_ip.json()['result']['content']

            # Discover your public IP address.
            my_ip = requests.get("https://api.ipify.org?format=json").json()['ip']

            if ip != my_ip:
                data = {"type": "A", "name": ddns, "content": my_ip}
                # Update the record
                req = requests.put(
                    "https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}".format(zone_identifier, dns_id),
                    data=json.dumps(data), headers={"X-Auth-Email": email, "X-Auth-Key": authkey,
                                                    "Content-Type": "application/json"})
                log('DNS ip address has been changed')
        except Exception as e:
            log(e, error=True)

        time.sleep(60)
